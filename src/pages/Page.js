import classnames from "classnames";
import PropTypes from "prop-types";
import { memo } from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import "./Page.scss";

function Page({ className, children }) {
  return (
    <Container className={classnames("page", className)} fluid>
      <Row className="no-gutters">
        <Col>{children}</Col>
      </Row>
    </Container>
  );
}

Page.propTypes = {
  className: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.string,
  ]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.string,
  ]),
};

export default memo(Page);
