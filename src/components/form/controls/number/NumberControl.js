import classnames from "classnames";
import numeral from "numeral";
import PropTypes from "prop-types";
import { forwardRef, memo, useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import "./NumberControl.scss";

const KEY_UP = 38,
  KEY_DOWN = 40,
  format = "-0[.][000000000000000000]";

const NumberControl = forwardRef((props, ref) => {
  const { className, value, min, max, onChange } = props,
    [formattedValue, setFormattedValue] = useState(formatValue(value)),
    [prevFormattedValue, setPrevFormattedValue] = useState(formatValue(value));

  function formatValue(value) {
    if (!value || value === "0") {
      return value;
    }

    const formattedValue = numeral(Math.max(min, Math.min(value, max))).format(
      format
    );

    if (/\.$/.test(value)) {
      const parts = value.toString().match(/\.\d*$/);

      return `${formattedValue}${parts.length > 0 ? parts[0] : "."}`;
    }

    return formattedValue;
  }

  function handleChange({ target: { value } }) {
    setFormattedValue(formatValue(value));
  }

  function handleKeyDown({ keyCode }) {
    let value = parseFloat(formattedValue) || 0;

    switch (keyCode) {
      case KEY_UP:
        value++;
        break;
      case KEY_DOWN:
        value--;
        break;
      default:
        break;
    }

    setFormattedValue(formatValue(value));
  }

  useEffect(() => {
    if (prevFormattedValue !== formattedValue) {
      onChange && onChange(formattedValue);
      setPrevFormattedValue(formattedValue);
    }
  }, [prevFormattedValue, formattedValue, onChange]);

  return (
    <Form.Control
      {...props}
      ref={ref}
      className={classnames("number-control", className)}
      type="text"
      placeholder="0"
      value={formattedValue}
      onChange={handleChange}
      onKeyDown={handleKeyDown}
    />
  );
});
NumberControl.displayName = "NumberControl";
NumberControl.propTypes = {
  className: PropTypes.string,
  value: PropTypes.number,
};

NumberControl.defaultProps = {
  min: -Infinity,
  max: Infinity,
};

export default memo(NumberControl);
