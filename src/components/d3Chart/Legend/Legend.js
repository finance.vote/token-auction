import classnames from "classnames";
import { memo } from "react";
import Arrow from "../../graph/grid/arrow.svg";
import Text from "../../graph/grid/text.svg";
import { BLOCK_TYPES } from "../helpers";
import styles from "./Legend.module.scss";

const GridLegendItem = memo(({ x = "0", y = 0, type, label }) => {
  return (
    <g
      className={"grid-legend-item"}
      transform={"translate(" + x + ", " + y + ")"}
    >
      <rect
        width="10px"
        height="10px"
        className={classnames(styles.point, styles[type])}
      />
      {type === BLOCK_TYPES.CLOSED && (
        <text
          className={styles.xIcon}
          dominantBaseline="middle"
          textAnchor="middle"
          lengthAdjust={"spacing"}
          x={5}
          y={6}
        >
          X
        </text>
      )}
      <text className={styles.text} x={17} y="0.6rem">
        {label}
      </text>
    </g>
  );
});
const ScrollArrow = () => {
  return (
    <foreignObject width={120} height={150}>
      <div
        style={{
          position: "absolute",
          left: 0,
          display: "flex",
          flexFlow: "column",
          top: "5rem",
        }}
      >
        <img src={Arrow} alt="arrow_right" />
        <img src={Text} alt="text_arrow" />
      </div>
    </foreignObject>
  );
};

const Legend = memo(({ x = 0, y = 0 }) => {
  return (
    <g className={"grid-legend"} transform={`translate(${x}, ${y})`}>
      <GridLegendItem y="0" type={BLOCK_TYPES.OPEN} label="Sold" />
      <GridLegendItem y="24" type={BLOCK_TYPES.CLOSED} label="Lot sold out" />
      <GridLegendItem y="48" type={BLOCK_TYPES.ACTIVE} label="Buy" />
      <ScrollArrow />
    </g>
  );
});

Legend.displayName = "Legend";
GridLegendItem.displayName = "GridLegendItem";

export default Legend;
