import classnames from "classnames";
import { max } from "d3";
import { forwardRef, memo, useCallback, useState } from "react";
import { isMobile } from "react-device-detect";
import { BLOCK_TYPES } from "./helpers";
import styles from "./Rectangles.module.scss";
import Tooltip from "./Tooltip/Tooltip";
//Specific classnames for blocktypes
const getBlockType = (item, index, arrLength, isNextBlock) => {
  if (index === arrLength - 1) {
    return BLOCK_TYPES.ACTIVE;
  }
  if (item.tokensLeftInTranche === 0 && isNextBlock) {
    return BLOCK_TYPES.CLOSED;
  } else {
    return BLOCK_TYPES.OPEN;
  }
};
const VolumeBar = memo(
  ({ xCoords, actualHeight, rangeY, blockHeightWithPadding }) => {
    const translateY =
      actualHeight - blockHeightWithPadding >= 0
        ? `-${actualHeight - blockHeightWithPadding}`
        : `${actualHeight - blockHeightWithPadding}`;
    return (
      <rect
        className={styles.dataVolume}
        x={xCoords}
        y={rangeY}
        width={10}
        height={actualHeight}
        transform={`translate(0,${translateY})`}
      />
    );
  },
  (prev, next) => JSON.stringify(prev) === JSON.stringify(next)
);

VolumeBar.displayName = "VolumeBar";

const boxWidth = isMobile ? 220 : 330;
const boxHeight = 110;
const RectanglesGroup = memo(
  ({
    xCoords,
    yCoords,
    rangeY,
    rangeX,
    actualHeight,
    blockType,
    element,
    xPan,
    soldGroup,
    clearTooltipDataAndHide,
    fillTooltipWithData,
    blockHeightWithPadding,
  }) => {
    const y = rangeY - yCoords > boxHeight ? yCoords : yCoords - boxHeight;
    const x =
      rangeX - xCoords - xPan < boxWidth
        ? xCoords - (boxWidth - boxHeight)
        : xCoords;
    return (
      <>
        <g>
          <VolumeBar
            {...{ xCoords, actualHeight, rangeY, blockHeightWithPadding }}
          />
        </g>
        <g
          className={styles.rectangle}
          onMouseEnter={() => fillTooltipWithData(x, y, element)}
          onMouseOut={clearTooltipDataAndHide}
        >
          <rect
            x={xCoords}
            y={yCoords}
            width="10px"
            height="10px"
            className={classnames(styles.point, styles[blockType])}
          />
          {soldGroup ? (
            <text
              className={styles.text}
              dominantBaseline="middle"
              textAnchor="middle"
              lengthAdjust={"spacing"}
              x={xCoords + 10 / 2}
              y={yCoords + 10 / 1.7}
            >
              X
            </text>
          ) : null}
        </g>
      </>
    );
  },
  (prevProps, nextProps) => {
    return JSON.stringify(prevProps) === JSON.stringify(nextProps);
  }
);

RectanglesGroup.displayName = "RectanglesGroup";

const Rectangles = forwardRef(
  ({ data, scale, rangeX, rangeY, xPan, blockHeightWithPadding }, ref) => {
    const [visibleTooltip, setVisibleTooltip] = useState(false);
    const [tooltipData, setTooltipData] = useState({});
    const fillTooltipWithData = useCallback((x, y, element) => {
      setTooltipData({
        x,
        y,
        ...element.tooltip,
      });
      setVisibleTooltip(true);
    });
    const clearTooltipDataAndHide = () => {
      setTooltipData({});
      setVisibleTooltip(false);
    };
    return (
      <g ref={ref}>
        {scale.y &&
          data?.map((event, index) => {
            const volumeMax = max(data, (data) => data.volume);
            const { x, y, volume } = event;
            const xCoords = scale.x(x);
            const yCoords = scale.y(y);
            const actualHeight = (volume / volumeMax) * rangeY * 0.8;
            const isNextBlock = index + 1 !== data.length;
            const blockType = getBlockType(
              event,
              index,
              data.length,
              isNextBlock
            );
            return (
              <RectanglesGroup
                key={index}
                {...{
                  xCoords,
                  yCoords,
                  rangeY,
                  rangeX,
                  actualHeight,
                  blockType,
                  element: event,
                  xPan,
                  soldGroup: !!(event.tokensLeftInTranche === 0 && isNextBlock),
                  clearTooltipDataAndHide,
                  fillTooltipWithData,
                  blockHeightWithPadding,
                }}
              />
            );
          })}
        {visibleTooltip ? <Tooltip {...tooltipData} /> : null}
      </g>
    );
  }
);
Rectangles.displayName = "Rectangles";
export default memo(Rectangles);
