import classnames from "classnames";
import { select } from "d3-selection";
import { forwardRef, useEffect } from "react";
import styles from "./Axis.module.scss";

const Axis = forwardRef(({ scale, transform, yScale }, ref) => {
  useEffect(() => {
    const node = ref.current;
    const selectedNode = select(node);
    selectedNode.call(scale);
  }, [scale]);
  return (
    <g
      ref={ref}
      transform={transform}
      className={classnames("main axis", yScale && styles.yAxis)}
    />
  );
});
Axis.displayName = "Axis";
export default Axis;
