import { sortObjects } from "components/d3Chart/helpers";
import Spinner from "components/indicators/spinner";
import { useGlobalState } from "globalState";
import { useEventsV2, useInitialPrice } from "queries/contract";
import { useEffect, useState } from "react";
// import { subscribeToPurchaseEvent } from "utils/ethereum/contract";
import { formatNumber } from "utils/formats";
import networks from "utils/networks.json";
import { generateDataSets, mapBlockY } from "./helpers";
import Legend from "./Legend";
import NewChart from "./NewChart";

const labelFormat = "0,0.000000";

const NewChartPage = ({ onError }) => {
  const [currentNetwork] = useGlobalState("currentNetwork");
  const tokenSymbol = networks[currentNetwork].symbol;
  // const { isLoading, error, events } = useEvents();

  const { isLoading, error, data: events } = useEventsV2();
  const { data: initialPrice } = useInitialPrice();
  const [data, setData] = useState();
  const _marksArr = [{ value: 0, label: `0 ${tokenSymbol}`, y: 0 }];
  for (let i = 0; i < 13; i++) {
    const value = 2 ** i * initialPrice;
    const label = formatNumber(value, labelFormat) + ` ${tokenSymbol}`;
    _marksArr.push({ value: value, label, y: i + 1 });
  }
  useEffect(() => {
    if (isLoading || !events?.length || !initialPrice) {
      return;
    }
    const nonEmptyEvents = events.filter((x) => parseFloat(x.ethSpent) > 0);
    const data = sortObjects(
      nonEmptyEvents.map((event) => {
        const { price, timestamp, trancheNumber, tokensAcquired, ethSpent } =
          event;
        const unquantizedPrice = parseFloat(price.unquantized);
        const tokensLeftInTranche = parseFloat(event.tokensLeftInTranche);
        const formatted = formatNumber(
          unquantizedPrice.toFixed(7),
          labelFormat
        );
        const idx = _marksArr.findIndex(
          (item) => item.value === Number(formatted)
        );
        const mark = _marksArr[idx];
        const y = mark?.y ?? mapBlockY(_marksArr, formatted);
        return {
          trancheNumber,
          tokensLeftInTranche,
          isEmptyTranche: tokensLeftInTranche <= 0,
          volume: parseInt(tokensAcquired),
          tooltip: {
            timestamp: timestamp * 1000,
            trancheNumber: trancheNumber,
            amount: tokensAcquired,
            price: price.unquantized,
            cost: ethSpent,
            symbol: tokenSymbol,
          },
          x: parseInt(timestamp),
          y,
          yLabel: unquantizedPrice,
        };
      }),
      "x, trancheNumber, isEmptyTranche"
    ).map((event, index, _arr) => {
      const isLastOne = event === _arr.at(-1);
      return {
        ...event,
        x: index,
        isLastOne,
      };
    });
    setData(data);
  }, [events, initialPrice]);
  useEffect(() => {
    onError(error);
  }, [error]);
  // useEffect(() => {
  //   subscribeToPurchaseEvent(() => refetch());
  // }, []);
  return (
    <div style={{ width: "100%", height: "100%", position: "relative" }}>
      {isLoading || !data?.length ? (
        <Spinner />
      ) : (
        <>
          <NewChart
            data={{ datasets: generateDataSets(data, _marksArr.length) }}
            marks={_marksArr}
          />
          <Legend />
        </>
      )}
    </div>
  );
};

export default NewChartPage;
