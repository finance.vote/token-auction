import ThreeDots from "components/indicators/three-dots";
import { useGlobalState } from "globalState";
import moment from "moment";
import { useEventsV2 } from "queries/contract";
import { memo, useMemo, useRef } from "react";
import Table from "react-bootstrap/Table";
import { Scrollbars } from "react-custom-scrollbars";
import { useTable } from "react-table";
import { formatNumber, FormattedHash } from "utils/formats";
import networks from "utils/networks.json";
import "./AuctionTable.scss";

const ScrollbarThumb = (props) => (
  <div {...props} className="auction-table__scrollbars__thumb" />
);

function AuctionTable() {
  const [currentNetwork] = useGlobalState("currentNetwork");
  const { symbol, explorer, availableToken } = networks[currentNetwork];
  const containerRef = useRef(null);
  const dataTableRef = useRef(null);
  // const { isLoading, error, events } = useEvents();
  const { isLoading, error, data: events } = useEventsV2();
  const hasEvents = events?.length > 0;
  const resolvedEvents = useMemo(() => {
    const tempEvents = events?.sort((prevEv, nextEv) => {
      if (prevEv.timestamp > nextEv.timestamp) {
        return -1;
      } else if (prevEv.timestamp < nextEv.timestamp) {
        return 1;
      } else {
        return 0;
      }
    });
    return tempEvents?.map(
      ({ tokensAcquired, price, txHash, ethSpent, timestamp }) => ({
        [`${availableToken} Bought`]: formatNumber(tokensAcquired),
        Price: `${formatNumber(price.unquantized)} ${symbol}`,
        [`${symbol} Cost`]: `${ethSpent} ${symbol}`,
        "Tx Time (UTC)": moment(new Date(parseInt(timestamp) * 1000)).format(
          "DD/MM/YYYY HH:mm"
        ),
        "Tx Hash": (
          <a
            href={`${explorer}/tx/${txHash}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <FormattedHash hash={txHash} />
          </a>
        ),
      })
    );
  }, [events]);
  const { getTableProps, headerGroups, rows, prepareRow } = useTable({
    columns: useMemo(
      () =>
        hasEvents
          ? Object.keys(resolvedEvents[0]).map((header) => ({
              Header: header,
              accessor: header,
            }))
          : [],
      [hasEvents, resolvedEvents]
    ),
    data: resolvedEvents,
  });

  function handleScroll() {
    const container = containerRef.current,
      containerBounds = container.getBoundingClientRect(),
      dataTable = dataTableRef.current;

    dataTable.querySelectorAll("tr").forEach((row) => {
      const rowBounds = row.getBoundingClientRect();

      row.dataset.visible =
        rowBounds.y >= containerBounds.top &&
        rowBounds.y < containerBounds.bottom;
    });
  }

  if (error) {
    console.error(error);
  }

  if (isLoading) {
    return (
      <div>
        waiting for buy blocks
        <ThreeDots />
      </div>
    );
  }

  if (!hasEvents) {
    return <div>No data</div>;
  }

  return (
    <div className="auction-table" ref={containerRef}>
      <Table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup, index) => (
            <tr key={index} {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column, idx) => (
                <th key={idx} {...column.getHeaderProps()}>
                  {column.render("Header")}
                </th>
              ))}
            </tr>
          ))}
        </thead>
      </Table>
      <Scrollbars
        className="auction-table__scrollbars"
        style={{ height: "16rem" }}
        renderThumbHorizontal={ScrollbarThumb}
        renderThumbVertical={ScrollbarThumb}
        onScroll={handleScroll}
      >
        <Table
          {...getTableProps()}
          className="auction-table__data"
          ref={dataTableRef}
        >
          <tbody>
            {rows
              .filter((row) => {
                if (row.values["FACT Bought"].replaceAll(",", "") > 0) {
                  return true;
                }
                return false;
              })
              .map((row, rIndex) => {
                prepareRow(row);

                return (
                  <tr key={rIndex} {...row.getRowProps()}>
                    {row.cells.map((cell, cIndex) => (
                      <td key={cIndex} {...cell.getCellProps()}>
                        {cell.render("Cell")}
                      </td>
                    ))}
                  </tr>
                );
              })}
          </tbody>
        </Table>
      </Scrollbars>
    </div>
  );
}

export default memo(AuctionTable);
