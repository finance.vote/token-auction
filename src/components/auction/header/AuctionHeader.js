import React, { memo } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import HookedTokenDetailsBox from "components/containers/HookedTokenDetailsBox";
import {
  useTokenSupply,
  useTotalTokensOffered,
  useInitialPrice,
} from "queries/contract";
import AssetProfile from "../asset-profile";
import "./AuctionHeader.scss";

function AuctionHeader({ className, assetName, projectLogo, projectUrls }) {
  return (
    <Container className={classnames("auction-header", className)} fluid>
      <Row>
        <Col className="text-left">
          <AssetProfile
            className="auction-header__asset-profile"
            name={assetName}
            logo={projectLogo}
            urls={projectUrls}
          />
        </Col>
        <Col xs="auto" className="auction-header__token-details text-right">
          <HookedTokenDetailsBox
            className="auction-header__token-details__box"
            label="Tokens available"
            hook={useTotalTokensOffered()}
            valueFormat="0,0.[00000]"
          />
          <HookedTokenDetailsBox
            className="auction-header__token-details__box"
            label="Tokens supply"
            hook={useTokenSupply()}
            valueFormat="0,0.[00000]"
          />
          <HookedTokenDetailsBox
            className="auction-header__token-details__box"
            label="Starting price"
            hook={useInitialPrice()}
            valueFormat="0,0.[000000]"
            suffix="ETH"
          />
        </Col>
      </Row>
    </Container>
  );
}

AuctionHeader.propTypes = {
  className: PropTypes.string,
  assetName: PropTypes.string,
  projectLogo: PropTypes.element,
  projectUrls: PropTypes.object,
};

export default memo(AuctionHeader);
