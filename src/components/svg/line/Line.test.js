import React from "react";
import renderer from "react-test-renderer";
import Line from "./Line";

test("should match snapshot", () => {
  const tree = renderer.create(<Line />).toJSON();

  expect(tree).toMatchSnapshot();
});
