import React from "react";
import renderer from "react-test-renderer";
import TokenDetailsBox from "./TokenDetailsBox";

test("should match snapshot", () => {
  const tree = renderer
    .create(
      <TokenDetailsBox label="Tokens available" value={20000000} suffix="ETH" />
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
