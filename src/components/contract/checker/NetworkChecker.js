// import classnames from "classnames";
// import PropTypes from "prop-types";
// import { Fragment, memo, useCallback, useEffect, useState } from "react";
// import Alert from "react-bootstrap/Alert";

// import { events, metamaskWrongNetwork } from "utils/ethereum";
// import "./NetworkChecker.scss";

// function NetworkChecker({ className }) {
//   const [currentNetwork, setCurrentNetwork] = useState(null),
//     [show, setShow] = useState(false),
//     checkNetwork = useCallback(async () => {
//       try {
//         const { isOk, current } = await metamaskWrongNetwork();

//         setShow(!isOk);
//         setCurrentNetwork(current);
//       } catch (err) {
//         console.error(err);

//         setShow(true);
//       }
//     }, []);

//   useEffect(() => {
//     checkNetwork();

//     events.addEventListener(events.EVENTS.NETWORK_CHANGE, checkNetwork);

//     return () => {
//       events.removeEventListener(events.EVENTS.NETWORK_CHANGE, checkNetwork);
//     };
//   }, [checkNetwork]);

//   if (show) {
//     return (
//       <Alert
//         className={classnames("network-checker", className)}
//         variant='warning'
//         dismissible
//         onClose={() => setShow(false)}
//       >
//         Opps, looks like we cannot connect to the blockchain. Your current
//         network is {currentNetwork || "unknown"}.
//         <br />
//         Check which network your ethereum wallet is connected to and refresh the
//         page.
//       </Alert>
//     );
//   }

//   return <Fragment />;
// }

// NetworkChecker.propTypes = {
//   className: PropTypes.string,
// };

// export default memo(NetworkChecker);
