import { useChainHeight, useStartBlock } from "queries/contract";
import { memo } from "react";

function StartBlock({ children }) {
  const { data: startBlock } = useStartBlock();
  const { data: chainHeight } = useChainHeight();
  if (chainHeight < startBlock) {
    return (
      <div>
        <p>
          You are early, please wait until the blockchain is at height:
          {startBlock}
        </p>
      </div>
    );
  }

  return children;
}

export default memo(StartBlock);
