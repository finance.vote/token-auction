import React from "react";
import renderer from "react-test-renderer";
import YAxis from "./YAxis";

test("should match snapshot", () => {
  const tree = renderer.create(<YAxis />).toJSON();

  expect(tree).toMatchSnapshot();
});
