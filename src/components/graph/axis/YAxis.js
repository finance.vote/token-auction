import { forwardRef, memo } from "react";
import Axis, { ORIENTATIONS } from "./Axis";

const YAxis = forwardRef((props, ref) => {
  return <Axis {...props} ref={ref} orientation={ORIENTATIONS.VERTICAL} />;
});
YAxis.displayName = "YAxis";
export default memo(YAxis);
