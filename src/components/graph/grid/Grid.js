import classnames from "classnames";
import { XAxis, YAxis } from "components/graph/axis";
import Block, { TYPES } from "components/graph/block";
import SVG from "components/svg/SVG";
import PropTypes from "prop-types";
import { Fragment, memo, useMemo, useRef, useState } from "react";
import { resolveCoordinate } from "utils/units";
import Arrow from "./arrow.svg";
import "./Grid.scss";
import Legend from "./legend";
import Text from "./text.svg";
import useResizer from "./useResizer";

function getArrayMinKeyValue(array, key) {
  return array.length > 0
    ? array.reduce(
        (accumulator, currentValue) => Math.min(accumulator, currentValue[key]),
        array[0][key]
      )
    : 0;
}

function getArrayMaxKeyValue(array, key) {
  return array.length > 0
    ? array.reduce(
        (accumulator, currentValue) => Math.max(accumulator, currentValue[key]),
        array[0][key]
      )
    : 0;
}

function Grid({
  className,
  data,
  height,
  width,
  movable,
  blockSpacing,
  blockSize,
  minX,
  maxX,
  minY,
  maxY,
  minYLabel,
  maxYLabel,
  yAxisMarkSuffix,
  showNextBlock,
  initialValue,
}) {
  const gridRef = useRef(null),
    legendRef = useRef(null),
    dataGridRef = useRef(null),
    xAxisRef = useRef(null),
    yAxisRef = useRef(null),
    [currentDataX, setCurrentDataX] = useState(0),
    [currentDataY, setCurrentDataY] = useState(0),
    rangeX = useMemo(() => maxX - minX, [minX, maxX]),
    rangeY = useMemo(() => maxY - minY, [minY, maxY]),
    rangeYLabel = useMemo(
      () =>
        (typeof maxYLabel === "undefined" ? maxY : maxYLabel) -
        (typeof minYLabel === "undefined" ? minY : minYLabel),
      [minY, maxY, minYLabel, maxYLabel]
    ),
    dataMinX = useMemo(() => getArrayMinKeyValue(data, "x"), [data]),
    dataMinY = useMemo(() => getArrayMinKeyValue(data, "y"), [data]),
    relativeData = useMemo(
      () =>
        data.map((props) => {
          const { x, y } = props;

          return {
            ...props,
            x:
              (x - dataMinX) * blockSize +
              (x - dataMinX) * blockSpacing +
              blockSpacing * 2,
            y:
              (y - dataMinY) * blockSize +
              (y - dataMinY) * blockSpacing +
              blockSpacing * 2 +
              blockSize,
          };
        }),
      [data, blockSize, blockSpacing, dataMinX, dataMinY]
    ),
    hasData = useMemo(
      () => relativeData && relativeData.length > 0,
      [relativeData]
    ),
    maxVolume = useMemo(
      () => getArrayMaxKeyValue(relativeData, "volume"),
      [relativeData]
    ),
    {
      resizeListener,
      dataBounds,
      resolvedWidth,
      resolvedHeight,
      legendX,
      legendY,
      currentMinX,
      currentMaxX,
      currentMinY,
      currentMaxY,
      currentMinYLabel,
      currentMaxYLabel,
      xAxisX,
      xAxisY,
      yAxisX,
      yAxisY,
    } = useResizer({
      dataGridRef,
      legendRef,
      xAxisRef,
      yAxisRef,
      currentDataX,
      currentDataY,
      width,
      height,
      rangeX,
      rangeY,
      rangeYLabel,
      blockSize,
      minX,
      maxX,
      minY,
      maxY,
    });

  function getBlockTypeFromProps({ tokensLeftInTranche }) {
    if (tokensLeftInTranche === 0) {
      return TYPES.CLOSED;
    }

    return TYPES.OPEN;
  }

  function handleDataMove({ x, y }) {
    setCurrentDataX(x);
    setCurrentDataY(y);
  }

  function handleBlockMouseOver({ currentTarget }) {
    currentTarget.parentElement.appendChild(currentTarget);
  }

  return (
    <div
      ref={gridRef}
      className={classnames("grid", className)}
      style={{ width, height, position: "relative" }}
    >
      {resizeListener}
      <SVG className="grid__axes" width={resolvedWidth} height={resolvedHeight}>
        <XAxis
          x={xAxisX}
          y={xAxisY}
          length={resolveCoordinate(resolvedWidth - xAxisX)}
          min={currentMinX}
          max={currentMaxX}
          showLabels={false}
          ref={xAxisRef}
        />
        <YAxis
          x={yAxisX}
          y={yAxisY}
          length={resolveCoordinate(resolvedHeight - 1)}
          min={currentMinY}
          minLabel={currentMinYLabel}
          max={currentMaxY}
          maxLabel={currentMaxYLabel}
          initialValue={initialValue}
          showLine={false}
          showLabels={false}
          showMarks={true}
          markSuffix={yAxisMarkSuffix}
          markSpacing={53}
          ref={yAxisRef}
        />
        <Legend ref={legendRef} x={`${legendX}px`} y={`${legendY}px`} />
      </SVG>
      <SVG>
        <Legend ref={legendRef} x={`${legendX}px`} y={`${legendY}px`} />
      </SVG>
      <div
        style={{
          position: "absolute",
          right: 0,
          display: "flex",
          flexFlow: "column",
          top: "5rem",
        }}
      >
        <img src={Arrow} alt="" />
        <img src={Text} alt="" />
      </div>
      <SVG
        className="grid__data"
        style={{
          marginTop: dataBounds.y,
          marginLeft: dataBounds.x,
        }}
        width={dataBounds.width}
        height={dataBounds.height}
        movable={movable}
        moveY={false}
        onMove={handleDataMove}
      >
        <g ref={dataGridRef}>
          <g>
            {relativeData &&
              relativeData.map(({ x, volume }, index) => {
                const maxHeight = resolvedHeight * 0.8,
                  y = resolvedHeight - (volume / maxVolume) * maxHeight;

                return (
                  <rect
                    key={`grid-volume-${index}`}
                    className="grid__data__volume-bar"
                    x={x}
                    y={y}
                    width={blockSize}
                    height={resolvedHeight - y}
                  />
                );
              })}
          </g>
          <g>
            {hasData ? (
              relativeData.map((props, index, { length }) => {
                const x = props.x,
                  y = resolvedHeight - props.y - blockSize - blockSpacing,
                  bounds = {
                    x: currentDataX - x,
                    y: currentDataY - y,
                    height: resolvedHeight - dataBounds.y,
                    width: resolvedWidth - dataBounds.x,
                  },
                  isFinalBlock = index === length - 1;

                return (
                  <Fragment key={`grid-block-${index}`}>
                    <Block
                      {...props}
                      type={getBlockTypeFromProps(props, index)}
                      x={x}
                      y={y}
                      width={blockSize}
                      height={blockSize}
                      tooltipBounds={bounds}
                      onMouseOver={handleBlockMouseOver}
                    />
                    {showNextBlock && isFinalBlock && (
                      <Block
                        x={x + blockSize + blockSpacing}
                        y={y}
                        width={blockSize}
                        height={blockSize}
                        type={TYPES.ACTIVE}
                      />
                    )}
                  </Fragment>
                );
              })
            ) : (
              <Block
                x={0}
                y={resolvedHeight - blockSize - blockSpacing}
                width={blockSize}
                height={blockSize}
                type={TYPES.ACTIVE}
              />
            )}
          </g>
        </g>
      </SVG>
    </div>
  );
}

Grid.propTypes = {
  className: PropTypes.string,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      x: PropTypes.number.isRequired,
      y: PropTypes.number.isRequired,
    })
  ),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  movable: PropTypes.bool,
  blockSpacing: PropTypes.number.isRequired,
  blockSize: PropTypes.number.isRequired,
  minX: PropTypes.number,
  maxX: PropTypes.number,
  minY: PropTypes.number,
  maxY: PropTypes.number,
  minYLabel: PropTypes.number,
  maxYLabel: PropTypes.number,
  yAxisMarkSuffix: PropTypes.string,
  showNextBlock: PropTypes.bool,
  initialValue: PropTypes.number,
};

Grid.defaultProps = {
  data: [],
  width: 400,
  height: 400,
  movable: true,
  blockSpacing: 2,
  blockSize: 10,
  showNextBlock: false,
};

export default memo(Grid);
