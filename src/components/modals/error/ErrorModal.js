import classnames from "classnames";
import PropTypes from "prop-types";
import { memo } from "react";
import Modal from "react-bootstrap/Modal";
import "./ErrorModal.scss";

function ErrorModal(props) {
  const { className, header, body, footer } = props,
    modalProps = { ...props };

  delete modalProps.header;
  delete modalProps.body;
  delete modalProps.footer;

  return (
    <Modal {...modalProps} className={classnames("error-modal", className)}>
      <Modal.Header closeButton>{header}</Modal.Header>
      <Modal.Body>{body}</Modal.Body>
      {footer && <Modal.Footer>{footer}</Modal.Footer>}
    </Modal>
  );
}

ErrorModal.propTypes = {
  className: PropTypes.string,
  header: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  body: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  footer: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};

export default memo(ErrorModal);
