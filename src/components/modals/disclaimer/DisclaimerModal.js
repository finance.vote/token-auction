import classnames from "classnames";
import PropTypes from "prop-types";
import { memo, useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import "./DisclaimerModal.scss";

function DisclaimerModal(props) {
  const [show, setShow] = useState(props.show);

  function handleAccept() {
    setShow(false);
  }

  function handleReject() {
    const { location } = window;

    location.href = "https://duckduckgo.com";
  }

  return (
    <Modal
      {...props}
      className={classnames("disclaimer-modal", props.className)}
      show={show}
    >
      <Modal.Header>
        <Modal.Title>
          <h2>Disclaimer</h2>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          Launch is free, public, open source software and a set of smart
          contracts written in Solidity that can be deployed to any EVM
          compatible blockchain. Before using the Launch Protocol, please review
          the FAQs and documentation (the “Documentation”) for a detailed
          explanation on how the Launch Protocol works.
        </p>
        <p>
          FactoryDAO is an entity. It does not own or lead Launch, but rather
          supports and develops the free, open-source protocol that is Launch.
          Portions of the Launch Protocol are made available under the GNU
          General Public License v3 -- and the disclaimers contained therein
          apply including that the Launch Protocol is provided “AS IS”, “WITH
          ALL FAULTS” and “AT YOUR OWN RISK”. FactoryDAO will not be liable for
          any liability or damages whatsoever associated with your use,
          inability to use, or your interaction with other users of, the Launch
          Protocol, including any direct, indirect, incidental, special,
          exemplary, punitive or consequential damages, loss of profits, Ether
          (ETH), Avalanche (AVAX) tokens, and any other cryptocurrencies or
          anything of value, including fiat currency.
        </p>
        <p>
          You acknowledge that the current version of the Launch Protocol is a
          beta version and as such has not been fully-tested and may not perform
          as designed. On the Launch Protocol, cryptographic tokens may be used
          for bidding on other cryptographic tokens. While you should always be
          thoughtful about the AVAX or other tokens/cryptocurrencies you use
          (and can lose) through the Launch Protocol, the concerns regarding
          loss of these tokens or cryptocurrencies is particularly acute with
          beta software that may not perform as designed, including that the
          beta version of the Launch Protocol may not accurately reflect the
          intent of the smart contracts, the FAQs or the Documentation. Your use
          of the Launch Protocol involves various risks, including, but not
          limited to losing tokens/cryptocurrencies due to invalidation. Careful
          due diligence should be undertaken as to the amount of AVAX or other
          tokens/ cryptocurrencies you bid using the Launch Protocol in beta
          format with full understanding that any bid of these
          tokens/cryptocurrencies could be subject to total loss. You assume any
          and all risk associated with your use of the Launch Protocol.
        </p>
        <p>
          FactoryDAO has not sought to list any tokens on any cryptocurrency
          exchanges. Tokens may be listed or delisted on various possible
          exchanges. FactoryDAO disavows any obligation with respect to the
          listing of tokens on exchanges and it disavows any responsibility with
          respect to the value or trading of tokens on exchanges. Persons
          trading tokens or otherwise engaged in activities with tokens on
          exchanges assume any and all risk, including that of total loss,
          associated with such activities.
        </p>
        <p>
          You are solely responsible for compliance with all laws that may apply
          to your particular use of the Launch Protocol. Cryptocurrencies and
          blockchain technologies have been the subject of discussion by various
          regulatory bodies around the world. FactoryDAO makes no representation
          regarding the application of any laws, including but by no means
          limited to those relating to gaming, options, derivatives or
          securities, to your use of the Launch Protocol. Indeed, depending on
          the jurisdiction and the contemplated use of the Launch Protocol
          (whether yours or another’s), that use may be considered illegal. You
          agree that FactoryDAO or any other referrer is not responsible for
          determining whether or which laws may apply to your use of the Launch
          Protocol. You may modify the Launch Protocol under the terms of the
          applicable open source license to effectuate your compliance with any
          applicable laws.
        </p>
      </Modal.Body>
      <Modal.Footer className="d-flex justify-content-center">
        <Button variant="primary" onClick={handleAccept}>
          I agree to the above
        </Button>
        <Button variant="lint btn-danger" onClick={handleReject}>
          Get me out of here
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

DisclaimerModal.propTypes = {
  className: PropTypes.string,
  show: PropTypes.bool,
  onHide: PropTypes.func,
};

DisclaimerModal.defaultProps = {
  onHide: () => false,
};

export default memo(DisclaimerModal);
