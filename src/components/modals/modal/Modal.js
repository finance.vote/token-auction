import classnames from "classnames";
import PropTypes from "prop-types";
import { memo } from "react";
import BSModal from "react-bootstrap/Modal";
import "./Modal.scss";
function Modal(props) {
  const { className, title, icon, header, children, footer } = props,
    modalProps = { ...props };

  delete modalProps.title;
  delete modalProps.icon;
  delete modalProps.header;
  delete modalProps.body;
  delete modalProps.footer;

  return (
    <BSModal {...modalProps} className={classnames("modal", className)}>
      <BSModal.Header closeButton />
      <BSModal.Body>
        <div className="modal__header">
          {icon && <img className="modal__header__icon" src={icon} alt="" />}
          {title && <h1>{title}</h1>}
          {header}
        </div>
        {children}
      </BSModal.Body>
      {footer && <BSModal.Footer>{footer}</BSModal.Footer>}
    </BSModal>
  );
}

Modal.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  icon: PropTypes.string,
  header: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  footer: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};

export default memo(Modal);
