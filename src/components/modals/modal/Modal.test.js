import React from "react";
import renderer from "react-test-renderer";
import Modal from "./Modal";

test("should match snapshot", () => {
  const tree = renderer.create(<Modal />).toJSON();

  expect(tree).toMatchSnapshot();
});
