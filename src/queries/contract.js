import { useEffect, useState } from "react";
import { useQuery } from "react-query";
import {
  buyToken,
  getCurrentPrice,
  getEvents,
  getEventsV2,
  getInitialPrice,
  getLotNumber,
  getStartBlock,
  getTokensLeftInLot,
  getTokenSupply,
  getTotalLots,
  getTotalTokensOffered,
  getTotalTokensSold,
  pushLiquidity,
  userIsManagement,
} from "../utils/curve";
import { getChainHeight } from "../utils/ethereum";

export const useBuyToken = () => buyToken;

export const usePushLiquidity = () => pushLiquidity;

export const useUserIsManagement = (address) =>
  useQuery(["userIsManagement", address], userIsManagement, {
    enabled: !!address,
  });

export const useLotNumber = () =>
  useQuery("lotNumber", getLotNumber, { refetchInterval: 15000 });

export const useTotalTokensSold = () =>
  useQuery("totalTokensSold", getTotalTokensSold, { refetchInterval: 15000 });

export const useTokenSupply = () => useQuery("tokenSupply", getTokenSupply);

export const useTotalTokensOffered = () =>
  useQuery("totalTokensOffered", getTotalTokensOffered);

export const useInitialPrice = () => useQuery("initialPrice", getInitialPrice);

export const useTokensLeftInLot = () =>
  useQuery("tokensLeftInLot", getTokensLeftInLot, { refetchInterval: 15000 });

export const useTotalLots = () => useQuery("totalLots", getTotalLots);

export const useStartBlock = () => useQuery("startBlock", getStartBlock);

export const useChainHeight = () =>
  useQuery("chainHeight", getChainHeight, { refetchInterval: 15000 });

export const useCurrentPrice = () =>
  useQuery("currentPrice", getCurrentPrice, { refetchInterval: 15000 });

export const useEvents = () => {
  const [isLoading, setIsLoading] = useState(true),
    [error, setError] = useState(null),
    [events, setEvents] = useState([]),
    [eventHandler, handleEvent] = useState(null);
  useEffect(() => {
    try {
      getEvents((event) => {
        setIsLoading(false);
        setEvents((events) => [event, ...events]);
        eventHandler && eventHandler(event);
      });
    } catch (err) {
      setError(err);
    }
  }, [eventHandler]);

  return {
    isLoading,
    error,
    events,
    handleEvent,
  };
};

export const useEventsV2 = () =>
  useQuery("allPurchaseEvents", getEventsV2, {
    refetchInterval: 15000,
    refetchOnWindowFocus: false,
  });
