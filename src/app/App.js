import Footer from "components/footer";
import Spinner from "components/indicators/spinner";
import DisclaimerModal from "components/modals/disclaimer";
import GlobalStateProvider from "globalState";
import { lazy, memo, Suspense, useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import { Route, Routes, useNavigate } from "react-router-dom";
import ErrorBoundry from "utils/ErrorBoundry";
import "./App.scss";
import Header from "./header";

const Auction = lazy(() => import("pages/auction"));
const HowItWorks = lazy(() => import("pages/how-it-works"));

function App() {
  const alreadyVisited = !!localStorage.getItem("token-launch-visited");
  const [firstVisit, setFirstVisit] = useState(!alreadyVisited);
  const navigate = useNavigate();
  function handleDisclaimerModalHide() {
    window.scrollTo(0, 0);
    localStorage.setItem("token-launch-visited", "true");
  }

  useEffect(() => {
    if (firstVisit) {
      setFirstVisit(false);
      navigate("/how-it-works", { replace: true });
    }
  }, [firstVisit, navigate]);

  return (
    <div className="app">
      <GlobalStateProvider>
        <DisclaimerModal
          show={!alreadyVisited}
          onExiting={handleDisclaimerModalHide}
        />
        <Container className="app__container">
          <Header className="app__header" />
          <ErrorBoundry>
            <Suspense fallback={<Spinner />}>
              <Routes>
                <Route path="/how-it-works" element={<HowItWorks />} />
                <Route path="/" element={<Auction />} />
              </Routes>
            </Suspense>
          </ErrorBoundry>
          <Footer />
        </Container>
      </GlobalStateProvider>
    </div>
  );
}

export default memo(App);
