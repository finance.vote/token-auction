import classnames from "classnames";
import Help from "components/icons/Help";
import ErrorModal from "components/modals/error";
import { ConnectWallet, ethInstance } from "evm-chain-scripts";
import { setGlobalState, useGlobalState } from "globalState";
import PropTypes from "prop-types";
import { usePushLiquidity, useUserIsManagement } from "queries/contract";
import { memo } from "react";
import Button from "react-bootstrap/Button";
import { NavLink } from "react-router-dom";
import "./Menu.scss";

function Menu({ className }) {
  const [address] = useGlobalState("address");
  const { data: userIsManagement } = useUserIsManagement(address);
  const pushLiquidity = usePushLiquidity();

  const handleConnect = async () => {
    try {
      const ethAccount = await ethInstance.getEthAccount(false);
      setGlobalState("address", ethAccount);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className={classnames("menu", className)}>
      <ErrorModal
        body={
          <>
            Failed to connect to your wallet.
            <br />
            Make sure you have a supported Ethereum wallet installed and
            enabled.
          </>
        }
        show={false}
        onHide={false}
      />
      {userIsManagement && (
        <Button variant="link" onClick={pushLiquidity}>
          Push Liquidity
        </Button>
      )}
      <ConnectWallet
        className="btn btn-link"
        listButtonClassName="wallet_listitem"
        headerClassName="wallet__header"
        containerClassName="wallet__container"
        addressContainerClassName={"wallet__hidden"}
        onConnect={handleConnect}
      />
      <NavLink component={Button} variant="link muted" to="/how-it-works">
        <Help />
      </NavLink>
    </div>
  );
}

Menu.propTypes = {
  className: PropTypes.string,
};

export default memo(Menu);
