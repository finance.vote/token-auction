window?.ethereum &&
  window.ethereum.on("chainChanged", () => window.location.reload());
window?.ethereum &&
  window.ethereum.on("accountsChanged", () => window.location.reload());

export {
  events,
  getAllEvents,
  getChainHeight,
  metamaskWrongNetwork,
} from "./contract";
export { isUserManagement } from "./management";
