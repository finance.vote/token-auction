import { ethInstance } from "evm-chain-scripts";

export async function isUserManagement(contract) {
  const [chainId, account] = await Promise.all([
    ethInstance.getWalletChainId(),
    ethInstance.getEthAccount(),
  ]);
  const resolvedContract = await ethInstance.getContract("read", contract, {
    chainId,
  });
  const managementAddr = await resolvedContract.management();
  return !!account && account === managementAddr;
}
