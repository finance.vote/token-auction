import { ethInstance, fromWei, toBN, toWei } from "evm-chain-scripts";
import Auction from "../truffle/Auction.json";
import { getAllEvents, isUserManagement } from "./ethereum";
import { getAllEventsV2 } from "./ethereum/contract";

async function purchaseEventCallback(event, callback, error) {
  if (error) {
    console.error(error);
  }
  const { transactionHash } = event;
  const [
    purchaser,
    weiSpent,
    tokensAcquired,
    tokensLeftInTranche, //weiReturned
    ,
    trancheNumber,
    timestamp,
  ] = event.args;
  const tokensPerEth =
    weiSpent > 0 ? toBN(tokensAcquired).div(toBN(weiSpent)) : 0;
  // const priceETH =
  //   tokensAcquired > 0
  //     ? toBN(weiSpent).mul(toBN(1e18)).div(toBN(tokensAcquired)).toNumber() /
  //       1e18
  //     : 0;
  const spent = Number(fromWei(weiSpent));
  const acquired = Number(fromWei(tokensAcquired));
  const priceETH = acquired > 0 ? spent / acquired : 0;
  const processedEvent = {
    price: {
      unquantized: priceETH.toString(), // in Szabo
      tokensPerEth: tokensPerEth.toString(), // in ETH
    },
    ethSpent: fromWei(weiSpent).toString(),
    tokensAcquired: fromWei(tokensAcquired).toString(),
    tokensLeftInTranche: fromWei(tokensLeftInTranche).toString(),
    trancheNumber: trancheNumber.toNumber(),
    purchaser,
    txHash: transactionHash,
    timestamp: timestamp.toNumber(),
  };
  // console.log("processedEvent", processedEvent);
  callback(processedEvent);
}

export function purchaseEventParser(event) {
  const { transactionHash, data } = event;
  const {
    purchaser,
    weiSpent,
    tokensAcquired,
    tokensLeftInTranche, //weiReturned
    trancheNumber,
    timestamp,
  } = data;
  const tokensPerEth =
    weiSpent > 0 ? toBN(tokensAcquired).div(toBN(weiSpent)) : 0;
  const spent = Number(fromWei(weiSpent));
  const acquired = Number(fromWei(tokensAcquired));
  const priceETH = acquired > 0 ? spent / acquired : 0;
  const processedEvent = {
    price: {
      unquantized: priceETH.toString(), // in Szabo
      tokensPerEth: tokensPerEth.toString(), // in ETH
    },
    ethSpent: fromWei(weiSpent).toString(),
    tokensAcquired: fromWei(tokensAcquired).toString(),
    tokensLeftInTranche: fromWei(tokensLeftInTranche).toString(),
    trancheNumber: Number(trancheNumber),
    purchaser,
    txHash: transactionHash,
    timestamp: Number(timestamp),
  };
  return processedEvent;
}

export async function buyToken(ethSpend, maxPrice) {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("write", Auction, { chainId });
  const weiSpend = toWei(ethSpend);
  const maxPriceWei = toWei(maxPrice);
  const tx = await contract.buy(maxPriceWei, {
    value: weiSpend,
  });
  const receipt = await tx.wait();
  const purchaseEvents = await contract.queryFilter(
    receipt.events[1],
    receipt.events[1].blockNumber,
    "latest"
  );
  const event = purchaseEvents.find(
    (tx) => tx.transactionHash === receipt.transactionHash
  );
  const tokensAcquired = event
    ? fromWei(event.args?.tokensAcquired).toString()
    : "0";
  return {
    tx: receipt.transactionHash,
    tokensAcquired,
  };
}

// ================ READ FUNCTIONS ===========================

export async function getEvents(callback) {
  const fromBlock = 14346112,
    toBlock = "latest";
  return await getAllEvents(
    (event) => purchaseEventCallback(event, callback),
    Auction,
    "PurchaseOccurred",
    fromBlock,
    toBlock
  );
}

export async function getEventsV2() {
  return await getAllEventsV2(Auction, "PurchaseOccurred");
}

export async function getCurrentPrice() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("read", Auction, { chainId }),
    priceWei = await contract.getBuyPrice();
  // console.log("priceWei", priceWei.toString());
  return fromWei(priceWei.toString());
}

export async function getTokensLeftInLot() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("read", Auction, { chainId }),
    tranche = await contract.currentTranche();
  return fromWei(tranche.currentTokens.toString());
}

export async function getInitialPrice() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("read", Auction, { chainId }),
    initialPrice = await contract.initialPrice();
  return fromWei(initialPrice.toString());
}

export async function getTotalTokensOffered() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("read", Auction, { chainId }),
    tokensForSale = await contract.totalTokensOffered();
  return fromWei(tokensForSale.toString());
}

export async function getTokenSupply() {
  //  const contract = await ethInstance.getContract('read', Token)
  //      , tokenSupply = await contract.totalSupply()
  //  return fromWei(tokenSupply.toString())
  // just hardcode it to remove build json dependency
  return "1000000000";
}

export async function getTotalTokensSold() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("read", Auction, { chainId }),
    tokensSold = await contract.totalTokensSold();
  return fromWei(tokensSold.toString());
}

export async function getLotNumber() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("read", Auction, { chainId });
  const [trancheNumber, totalTokensOffered, totalTokensSold] =
    await Promise.all([
      contract.trancheNumber(),
      contract.totalTokensOffered(),
      contract.totalTokensSold(),
    ]);
  if (
    fromWei(totalTokensOffered.toString()) -
      fromWei(totalTokensSold.toString()) ===
    0
  )
    return -1;
  return trancheNumber.toNumber();
}

export async function getInitialLotSize() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("read", Auction, { chainId }),
    size = await contract.initialTrancheSize();
  return fromWei(size.toString());
}

export async function getTotalLots() {
  const chainId = await ethInstance.getWalletChainId();
  const size = await getInitialLotSize(chainId),
    total = await getTotalTokensOffered(chainId);
  return Math.ceil(Math.log2(parseInt(total) / parseInt(size)));
}

export async function getStartBlock() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("read", Auction, { chainId }),
    startBlock = await contract.startBlock();
  return startBlock.toNumber();
}

// ================ MANAGEMENT FUNCTIONS ===========================

export async function userIsManagement() {
  return await isUserManagement(Auction);
}

export async function pushLiquidity() {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("write", Auction, { chainId }),
    account = await ethInstance.getEthAccount();

  return await contract.pushLiquidity({ from: account, gas: 3000000 });
}

export async function setSiteHash(hash) {
  const chainId = await ethInstance.getWalletChainId();
  const contract = await ethInstance.getContract("write", Auction, { chainId }),
    account = await ethInstance.getEthAccount();
  return await contract.setSiteHash(hash, { from: account });
}
