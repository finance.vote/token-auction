export const haltEvent = (event) => {
  event.stopPropagation();
  event.cancelable && event.preventDefault();
};

export const getCurrentPosition = ({ pageX, pageY }) => ({
  x: pageX,
  y: pageY,
});

export const eventToPosition = (event) =>
  getCurrentPosition(event.touches ? event.touches.item(0) : event);
