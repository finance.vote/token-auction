import PropTypes from "prop-types";
import { Component } from "react";

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      errorInfo: null,
    };
  }

  static getDerivedStateFromError(error) {
    return { error };
  }

  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo);
    this.setState({ error, errorInfo });
  }

  getDevError(error) {
    return error.stack ? error.stack : error.toString();
  }

  render() {
    const {
      props: { children },
      state: { error, errorInfo },
    } = this;

    if (error) {
      return (
        <>
          <h1>Something went wrong.</h1>
          <pre>
            {errorInfo
              ? errorInfo.componentStack && (
                  <>
                    {error.toString()}
                    {errorInfo.componentStack.toString()}
                  </>
                )
              : error.stack &&
                process.env.NODE_ENV !== "production" &&
                this.getDevError(error)}
          </pre>
        </>
      );
    }

    return children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
};

export default ErrorBoundary;
